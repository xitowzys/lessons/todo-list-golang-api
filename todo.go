package todoList

type TodoLists struct {
	Id          int    `json:"id"`
	Title       string `json:"title"`
	Descroption string `json:"description"`
}

type UsersList struct {
	Id     int
	UserId int
	ListId int
}

type TodoItem struct {
	Id          int    `json:"id"`
	Title       string `json:"title"`
	Descroption string `json:"descroption"`
	Done        bool   `json:"done"`
}

type ListsItem struct {
	Id     int
	ListId int
	ItemId int
}
