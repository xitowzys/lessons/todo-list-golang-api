package main

import (
	"gitlab.com/xitowzys/lessons/todo-list-golang-api"
	"gitlab.com/xitowzys/lessons/todo-list-golang-api/pkg/handler"
	"gitlab.com/xitowzys/lessons/todo-list-golang-api/pkg/repository"
	"gitlab.com/xitowzys/lessons/todo-list-golang-api/pkg/service"
	"log"
)

func main() {

	repos := repository.NewRepository()
	services := service.NewService(repos)
	handlers := handlers.NewHandler(services)

	srv := new(todoList.Server)

	if err := srv.Run("8888", handlers.InitRoutes()); err != nil {
		log.Fatalf("error occured while running http server: %s", err.Error())
	}

}
