package handlers

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/xitowzys/lessons/todo-list-golang-api/pkg/service"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()

	auth := router.Group("/auth")
	{
		auth.POST("/sign-up", h.signUp)
		auth.POST("/sign-in", h.signIn)
	}

	api := router.Group("/api")
	{
		lists := api.Group("/lists")
		{
			lists.POST("/", h.createItem)
			lists.GET("/", h.getAllLists)
			lists.GET("/:id", h.getListById)
			lists.PUT("/:id", h.updateItem)
			lists.DELETE("/:id", h.deleteItem)

			items := lists.Group(":id/items")
			{
				items.POST("/", h.createList)
				items.GET("/", h.getAllLists)
				items.GET("/:item_id", h.getListById)
				items.PUT("/:item_id", h.updateList)
				items.DELETE("/:item_id", h.deleteList)
			}
		}
	}

	return router

}
